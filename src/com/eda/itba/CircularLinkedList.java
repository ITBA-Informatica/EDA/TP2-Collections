package com.eda.itba;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularLinkedList<E> {
    private static class Node<E> {
        E value;
        Node<E> prev;
        Node<E> next;
        Node(Node<E> prev, E element, Node<E> next) {
            this.value= element;
            this.next = next;
            this.prev = prev;
        }
    }

    private Node<E> head = new Node<>(null,null,null);
    private int length;

    public boolean add(E element) {

        if (isEmpty()) {
            head.next = new Node<E>(head, element, head);;
            head.prev = head.next;
            length++;
            return true;
        } else {
            head.prev.next = new Node<E>(head.prev, element, head);
            head.prev = head.prev.next;
            length++;
            return true;
        }
    }

    public boolean remove(E element) {
        if (isEmpty()) return false;
        if (size() == 1 && head.next.value.equals(element)) {
            head.next = null;
            head.prev = null;
            length--;
            return true;
        }
        return removeRec(head.next, element);
    }

    public boolean removeRec(Node<E> node, E element) {
        if (node == head) return false;
        if (node.value.equals(element)) {
            node.next.prev = node.prev;
            node.prev.next = node.next;
            length--;
            return true;
        }
        return removeRec(node.next,element);
    }

    public int size() {
        return length;
    }

    public boolean isEmpty() {
        return length == 0;
    }


    public String toString() {
        if (isEmpty()) {
            return "[]";
        }
        return "[" + toStringRec(head.next);

    }

    private String toStringRec(Node node) {
        if (node  == head) {
            return "]";
        }
        if (node == head.next) return String.valueOf(node.value) + toStringRec(node.next);
        return ", " + String.valueOf(node.value) + toStringRec(node.next);
    }

    public String toStringReverse() {
        if (isEmpty()) {
            return "[]";
        }
        return "[" + toStringRecReverse(head.prev);
    }

    private String toStringRecReverse(Node node) {
        if (node  == head) {
            return "]";
        }
        if (node == head.prev) return String.valueOf(node.value) + toStringRecReverse(node.prev);
        return ", " + String.valueOf(node.value) + toStringRecReverse(node.prev);
    }

    public CircularLinkedListIterator<E> iterator() {
        return new CircularLinkedListIterator<>(this);
    }

    public static class CircularLinkedListIterator<E> implements Iterator<E> {
        private Node<E> current;
        private CircularLinkedList<E> list;

        public CircularLinkedListIterator(CircularLinkedList<E> list) {
            if (list.isEmpty()) {
                current = list.head;
            } else {
                current = list.head.next;
            }
            this.list = list;
        }
        public boolean hasNext() {
            return current != list.head;
        }

        public E next() {
            if (!hasNext()) throw new NoSuchElementException();
            current = current.next;
            return current.prev.value;
        }

        public E peek() {
            return current.value;
        }
    }
}
