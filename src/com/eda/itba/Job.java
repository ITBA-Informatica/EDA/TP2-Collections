package com.eda.itba;

import java.util.Objects;

public class Job {
    private int remainingTime;
    private int id;
    public Job() {
//        Los trabajos van a tener un tiempo de 2 a 30 segs al azar

        remainingTime = (int) Math.floor(2 + Math.random() * 28);
    }

    public void process(int processingTime) {

    }

    public int remaining() {
        return remainingTime;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String toString() {
        return String.valueOf("[" + String.valueOf(id) + "]" + " " +remainingTime);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Job job = (Job) o;
        return id == job.id;
    }

    public int process() {
        remainingTime--;
        return remainingTime;
    }
}
