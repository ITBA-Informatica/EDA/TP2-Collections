package com.eda.itba;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class LinearLinkedList<T> {

    private Node head;
    private int length;

    private class Node {
        private T value;
        private Node next;
        public int hashCode() {return value.hashCode();}
        Node(T value) {
            this.value = value;
        }
    }

////////////////// Metodos del EJ 2 //////////////////

    public int count(Predicate<T> lambda) {
        return countRec(head, lambda);
    }

    private int countRec(Node node, Predicate<T> lambda) {
        if (node == null) return 0;
        int count = lambda.test(node.value)? 1:0;
        return count + countRec(node.next, lambda);
    }

    public LinearLinkedList<T> filter(Predicate<T> lambda) {
        return filterRec(head, lambda);
    }

    private LinearLinkedList<T> filterRec(Node node, Predicate<T> lambda) {
        if (node == null) return new LinearLinkedList<>();
        LinearLinkedList<T> list = filterRec(node.next, lambda);
        if (lambda.test(node.value)) list.add(node.value);
        return list;
    }


    public <Q> LinearLinkedList<Q> map(Function<T,Q> operation) {
        LinearLinkedList resp = new LinearLinkedList<Q>();
        mapRec(head, operation, resp);
        return resp;
    }

    private <Q> void mapRec(Node node, Function<T,Q> operation, LinearLinkedList<Q> newList) {
        if (node == null) return;
        newList.add(operation.apply(node.value));
        mapRec(node.next, operation, newList);
        return;
    }

    public <Q> Q inject(BiFunction<Q,T,Q> operation, Q firstVal) {
        return injectRec(head, operation, firstVal);
    }

    public <Q> Q injectRec(Node node, BiFunction<Q,T,Q> operation, Q currInjection) {
        if (node == null) return currInjection;
        return injectRec(node.next, operation, operation.apply(currInjection, node.value));
    }

    public String toString() {
        return "[" + toStringRec(head);
    }

    private String toStringRec(Node node) {
        if (node  == null) {
            return "]";
        }
        if (node == head) return String.valueOf(node.value) + toStringRec(node.next);
        return ", " + String.valueOf(node.value) + toStringRec(node.next);
    }

////////////////////////////////////////////////////////


////////////////// Metodos del EJ 4 //////////////////

    public LinearLinkedList reverse() {
        if (isEmpty()) return this;
        Node prevHead = head;
        reverseRec(head);
        prevHead.next = null;
        return this;
    }

    private void reverseRec(Node n) {
        if (n.next == null) {
            head = n;
            return;
        };
        reverseRec(n.next);
        n.next.next = n;
        return;
    }

////////////////////////////////////////////////////////


    public boolean isEmpty() {
        return head == null;
    }

    public int size() {
        return this.length;
    }

    public void clear() {
        this.head = null;
        this.length = 0;
    }

    @SuppressWarnings("unchecked")
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof LinearLinkedList))
            return false;
        LinearLinkedList otherList = (LinearLinkedList) obj;

        return equalsRec(head,otherList.head);

    }

    private boolean equalsRec(Node n, Node m) {
        if (n == null && m == null) return true;
        if (n == null && m != null) return false;
        if (m == null && n != null) return false;
        if (!n.value.equals(m.value)) return false;
        return equalsRec(n.next,m.next);
    }

    public boolean add(T value) {
        Node node = new Node(value);
        if (isEmpty()) {
            head = node;
            length++;
            return true;
        } else {
            return addRec(node,head);
        }
    }

    private boolean addRec(Node newNode, Node node) {
        if (node.next == null) {
            node.next = newNode;
            length++;
            return true;
        } else {
            return addRec(newNode, node.next);
        }
    }


    //    Aca no tira un ClassCastException cuando le mando un objeto de otra clase, revisar
    public boolean contains(Object value) {
        return containsRec(value, head);
    }

    public boolean containsRec(Object value, Node node) {
        if (node == null) return false;
        if (node.value.equals(value)) return true;
        return containsRec(value, node.next);
    }

    @SuppressWarnings("unchecked")
    public boolean remove(Object o) {
        if (isEmpty()) return false;
        if (head.value.equals(o)) {
            head = head.next;
            length--;
            return true;
        }
        return removeRec(o, head);
    }

    private boolean removeRec(Object o, Node node) {
        if (node.next == null) return false;
        if (node.next.value.equals(o)) {
            length--;
            node.next = node.next.next;
            return true;
        }
        return removeRec(o,node.next);
    }

}
