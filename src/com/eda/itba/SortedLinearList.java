package com.eda.itba;

import java.util.*;

public class SortedLinearList<T> {
    private Node<T> head;
    private int length;

    private Comparator<T> cmp;

    private static class Node<E> {
        private E value;
        private Node<E> next;
        public int hashCode() {return value.hashCode();}
        Node(E value) {
            this.value = value;
        }
    }

    public SortedLinearList(Comparator<T> cmp) {
        this.cmp = cmp;
    }


    public SortedLinearListIterator<T> iterator() {
        return new SortedLinearListIterator<T>(this);
    }

    public static class SortedLinearListIterator<S> {
        private Node<S> current;

        SortedLinearListIterator(SortedLinearList<S> sortedList) {
            current = sortedList.head;
        }

        public boolean hasNext() {
            return current != null;
        }

        public S next() {
            if (!hasNext()) throw new NoSuchElementException();
            Node<S> aux = current;
            current = current.next;
            return aux.value;
        }

        public S peek() {
            if (!hasNext()) return null;
            return current.value;
        }
    }


    // Imagino que todas las listas estan ordenadas con el mismo comparador.
    // Sino el orden de complejidad aumentaría, y no tengo ganas de hacer el checkeo de que todos
    // tengan el mismo, asi que agarro el del primero

//    La idea seria tener un array de los N iteradores, y ver a cual le toca ir en el siguiente turno
    // Esto en teoría ahorra complejidad pero creo que termina quedando complejidad N * M donde N es la cantidad de
    // listas a unir, y M es la cantidad de items en las listas

//    Quedo un poco feo pero funciona

    public static <T> SortedLinearList<T> join(Collection<SortedLinearList<T>> listCollection) {
        SortedLinearList<T> newList = new SortedLinearList<>(listCollection.iterator().next().getCmp());
        LinkedList<SortedLinearListIterator<T>> iterators = new LinkedList<>();
//        Genero una lista con iteradores
        listCollection.forEach((l) -> iterators.add(l.iterator()));

        while(joinRec(null, newList, iterators.iterator()) != null) {
//            Don't do anything
        }
        return newList;
    }

    private static <T> T joinRec(T lowestValue, SortedLinearList<T> newList, Iterator<SortedLinearListIterator<T>> iteratorsIterator) {
        if (!iteratorsIterator.hasNext()) {
//            System.out.println("lowest value: " + lowestValue);
            return lowestValue;
        }
        SortedLinearListIterator<T> currIterator = iteratorsIterator.next();
        T currIteratorVal = currIterator.peek();
        if (currIteratorVal != null) {
            if (lowestValue == null) {
                lowestValue = currIteratorVal;
            } else if ((newList.getCmp().compare(currIteratorVal,  lowestValue) < 0)) {
                lowestValue = currIteratorVal;
            }
        }
        T realLowestValue = joinRec(lowestValue, newList, iteratorsIterator);
        if (currIteratorVal != null && currIteratorVal.equals(realLowestValue)) {
            newList.add(currIterator.next());
        }
        return realLowestValue;
    }
    public Comparator<T> getCmp() {
        return cmp;
    }

    public boolean add(T value) {
        Node<T> newNode = new Node<T>(value);
        if (isEmpty()) {
            head = newNode;
            return true;
        }
        if (cmp.compare(value,head.value) <= 0) {
            newNode.next = head;
            head = newNode;
        } else {
            addRec(head, newNode);
        }
        return true;
    }


    private void addRec(Node<T> n, Node<T> newNode) {
        if (n.next == null) {
            n.next = newNode;
            return;
        }
        if (cmp.compare(newNode.value, n.next.value) <= 0) {
            newNode.next = n.next;
            n.next = newNode;
        } else {
            addRec(n.next, newNode);
        }
    }

    public boolean isEmpty() {
        return head == null;
    }





    public void setCmp(Comparator<T> cmp) {
        this.cmp = cmp;
    }

    public String toString() {
        return "[" + toStringRec(head);
    }

    private String toStringRec(Node node) {
        if (node  == null) {
            return "]";
        }
        if (node == head) return String.valueOf(node.value) + toStringRec(node.next);
        return ", " + String.valueOf(node.value) + toStringRec(node.next);
    }


}
