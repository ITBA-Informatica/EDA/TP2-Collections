package com.eda.itba;

public class DoubleLinkedList<E> {

    private static class Node<E> {
        E value;
        Node<E> prev;
        Node<E> next;
        Node(Node<E> prev, E element, Node<E> next) {
            this.value= element;
            this.next = next;
            this.prev = prev;
        }
    }

    private Node<E> head;
    private Node<E> tail;
    private int length;

    public boolean add(E element) {

        if (isEmpty()) {
            head = new Node<E>(null, element, null);;
            tail = head;
            length++;
            return true;
        } else {
            return addRec(element, head);
        }
    }

    public boolean addRec(E element, Node<E> node) {
        if (node.next == null) {
            length++;
            node.next = new Node<E>(node, element, null);;
            tail = node.next;
            return true;
        }
        return addRec(element, node.next);
    }


    public boolean isEmpty() {
        return head == null;
    }


//    Para probar que los prevs hayan quedado bien se puede usar el metodo toStringPrevs, que va ciclando por los prevs
    public DoubleLinkedList<E> reverse() {
        if (isEmpty()) return this;
        if (size() == 1) return this;
        System.out.println("reversing");
        reverseRec(head);
        Node<E> newHead = tail;
        tail = head;
        head = newHead;
        return this;
    }

    private Node<E> reverseRec(Node<E> node) {
        if (node == null) return node;
        Node<E> newNext = node.prev;
        node.prev = reverseRec(node.next);
        node.next = newNext;
        return node;
    }

    public String toString() {
        return "[" + toStringRec(head);
    }

    private String toStringRec(Node node) {
        if (node  == null) {
            return "]";
        }
        if (node == head) return String.valueOf(node.value) + toStringRec(node.next);
        return ", " + String.valueOf(node.value) + toStringRec(node.next);
    }


//    Esta funcion checkea que los prevs hayan quedado bien despues del reverse
    public String toStringPrevs() {
        return "[" + toStringRecPrevs(tail);
    }

    private String toStringRecPrevs(Node node) {
        if (node  == null) {
            return "]";
        }
        if (node == tail) return String.valueOf(node.value) + toStringRecPrevs(node.prev);
        return ", " + String.valueOf(node.value) + toStringRecPrevs(node.prev);
    }


    public int size() {
        return length;
    }



}
