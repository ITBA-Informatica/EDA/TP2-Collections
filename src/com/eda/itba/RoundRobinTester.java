package com.eda.itba;

public class RoundRobinTester {
    public RoundRobinTester(int time, int jobRespawnRate) {
        RoundRobinProcessor processor = new RoundRobinProcessor();
        int elapsedTime = 0;
        while(elapsedTime < time) {
            if (elapsedTime % jobRespawnRate == 0) {
//                addJob
                Job newJob = new Job();
                processor.add(newJob);
            }
//            processJobs()
            processor.process();
            elapsedTime++;
            System.out.println("Elapsed Time: " + elapsedTime + " - Status: " + processor.toString());
        }
    }
}
