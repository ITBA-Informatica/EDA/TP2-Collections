package com.eda.itba;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class LinkedListSet<T> implements Set<T>{

    private class Node {
        private T value;
        private Node next;
        public int hashCode() {return value.hashCode();}
        Node(T value) {
            this.value = value;
        }
    }


    private int length = 0;
    private Node head;

    public boolean isEmpty() {
        return head == null;
    }

    public int size() {
        return this.length;
    }

    public void clear() {
        this.head = null;
        this.length = 0;
    }

    @SuppressWarnings("unchecked")
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof LinkedListSet))
            return false;
        LinkedListSet otherList = (LinkedListSet) obj;

        return equalsRec(head,otherList.head);

    }

    private boolean equalsRec(Node n, Node m) {
        if (n == null && m == null) return true;
        if (n == null && m != null) return false;
        if (m == null && n != null) return false;
        if (!n.value.equals(m.value)) return false;
        return equalsRec(n.next,m.next);
    }

    @Override
    public boolean add(T value) {

        Node node = new Node(value);
        if (isEmpty()) {
            head = node;
            length++;
            return true;
        } else if(head.value.equals(value)) {
            return false;
        } else {
            return addRec(node,head);
        }
    }

    private boolean addRec(Node newNode, Node node) {
        if (node.next == null) {
            node.next = newNode;
            length++;
            return true;
        } else if(node.next.value.equals(newNode.value)){
            return false;
        } else {
            return addRec(newNode, node.next);
        }
    }


//    Aca no tira un ClassCastException cuando le mando un objeto de otra clase, revisar
    public boolean contains(Object value) {
        return containsRec(value, head);
    }

    public boolean containsRec(Object value, Node node) {
        if (node == null) return false;
        if (node.value.equals(value)) return true;
        return containsRec(value, node.next);
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean remove(Object o) {
        if (isEmpty()) return false;
        if (head.value.equals(o)) {
            head = head.next;
            length--;
            return true;
        }
        return removeRec(o, head);
    }

    private boolean removeRec(Object o, Node node) {
        if (node.next == null) return false;
        if (node.next.value.equals(o)) {
            length--;
            node.next = node.next.next;
            return true;
        }
        return removeRec(o,node.next);
    }

    //    Todo
    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }



    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    //    End Todo


}
