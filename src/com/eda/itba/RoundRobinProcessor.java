package com.eda.itba;

import java.util.Iterator;

public class RoundRobinProcessor {
    private CircularLinkedList<Job> jobs = new CircularLinkedList<>();
    private int toAssignId;
    private int quantum = 10;
    private int timeSpentOnJob;
    private CircularLinkedList.CircularLinkedListIterator<Job> iterator = jobs.iterator();


    public boolean add(Job job) {
        job.setId(toAssignId++);
        jobs.add(job);
        System.out.println("Added Job [" + job.getId() + "] with time " + job.remaining());
        return true;
    }

    public String toString() {
        return jobs.toString();
    }

    public void process() {
        if (jobs.isEmpty()) return;
        if (!iterator.hasNext()) iterator = jobs.iterator();
        Job currentJob = iterator.peek();
        int remainingTime = iterator.peek().process();
        timeSpentOnJob++;
        if (remainingTime == 0) {
            System.out.println("Finished with processs [" + currentJob.getId() + "]");
            iterator.next();
            timeSpentOnJob = 0;
            jobs.remove(currentJob);
        } else if(timeSpentOnJob > quantum) {
            timeSpentOnJob = 0;
            iterator.next();
            System.out.println("Moved on(quantum) from processs [" + currentJob.getId() + "] with remaining time " + remainingTime);
        }

    }
}
