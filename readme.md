# TP 2 - Collections

## Consigna:

### 1. 
Realizar una implementación de la interfaz Set&lt;T&gt; que almacene internamente los elementos en
una lista lineal simplemente encadenada con header. Determinar el orden de complejidad temporal
requerido para realizar cada una de las operaciones.

### 2.
 Agregarle las siguientes operaciones a la implementación de LinearLinkedList&lt;T&gt; vista en
clase. Todas deben ser implementadas de manera recursiva.
- a. count: retorna la cantidad de elementos que cumplen con una determinada condición.

- b. filter: retorna una nueva lista con aquellos elementos que cumplen una condición.
- c. map: dada una función f: T → S, retorna una nueva lista de tipo S, con el resultado de
aplicarle la función a cada elemento de la lista.
- d. inject: dada una función f: S, T → S y un valor inicial de tipo S, retorna el resultado de
evaluar la función para cada elemento de la lista, utilizando como primer argumento el valor
retornado por la función en el nodo anterior, y como segundo argumento el valor del nodo
actual. Para el primer nodo usa como primer argumento el valor inicial.

### 3. 
Implementar un algoritmo que realice la unión de N listas lineales ordenadas simplemente
encadenadas sin repeticiones, y la retorne en una nueva lista.
### 4.
 Implementar un algoritmo recursivo que invierta una lista lineal simplemente encadenada.
### 5.
 Ídem al ejercicio anterior pero para el caso de una lista lineal doblemente encadenada.
### 6. 
El algoritmo de planificación round robin está especialmente diseñado para sistemas que
comparten procesador para atender varios trabajos. Funciona de la siguiente manera:
- Se define una constante, denominada quantum, que es el intervalo de tiempo máximo que
se puede atender a un trabajo en forma continua.
- Los trabajos que requieren procesador son insertados en una lista circular.
- El planificador recorre de manera ininterrumpida esta lista y le asigna procesador al primer
trabajo que encuentra. Si el tiempo que necesita dicho trabajo es menor o igual que el
quantum, el trabajo es atendido hasta el final de su proceso, y es borrado de la lista. De lo
contrario, se lo atiende hasta el quantum de tiempo, se le desasigna el procesador, y
continúa en la lista, actualizando su tiempo de procesamiento.

Escribir un programa que simule al planificador round robin. Para ello se debe recibir por línea de
comandos el tiempo total de la simulación, y cada cuánto tiempo quiere que se generen nuevos
trabajos. El quantum será una constante del programa y valdrá 10 unidades de tiempo. A medida
que un trabajo se inserta en la lista circular se le asocia el tiempo que necesita permanecer en el
procesador, generado en forma aleatoria. El programa debe imprimir por consola los nodos de la
lista circular en el tiempo y el proceso que es atendido en cada instante por el procesador.